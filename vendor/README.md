Place downloaded elisp files in ~/.emacs.d/vendor. You'll then be able
to load them.

For example, if you download yaml-mode.el to ~/.emacs.d/vendor,
then you can add the following code to this file:

```
(require 'yaml-mode)
(add-to-list 'auto-mode-alist '("\\.yml$" . yaml-mode))
```

Adding this code will make Emacs enter yaml mode whenever you open a .yml file

