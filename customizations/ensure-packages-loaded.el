;; this file should be read first before any other customizations
(princ "** ensure-packages-loaded.el -- Begin\n")

;; The packages you want installed. You can also install these
;; manually with M-x package-install
;; Add in your own as you wish:
(defvar my-packages
  '(;; makes handling lisp expressions much, much easier
    ;; Cheatsheet: http://www.emacswiki.org/emacs/PareditCheatsheet
    paredit

    ;; key bindings and code colorization for Clojure
    ;; https://github.com/clojure-emacs/clojure-mode
    clojure-mode

    ;; extra syntax highlighting for clojure
    clojure-mode-extra-font-locking

    ;; integration with a Clojure REPL
    ;; https://github.com/clojure-emacs/cider
    ;;cider

    ;; allow ido usage in as many contexts as possible. see
    ;; customizations/navigation.el line 23 for a description
    ;; of ido
    ido-completing-read+

    ;; Enhances M-x to allow easier execution of commands. Provides
    ;; a filterable list of possible commands in the minibuffer
    ;; http://www.emacswiki.org/emacs/Smex
    smex

    ;; project navigation
    projectile

    ;; colorful parenthesis matching
    rainbow-delimiters

    ;; edit html tags like sexps
    tagedit

    ;; git integration
    magit

    ;; you have to declare jabber-account-list in ~/.emacs.d/shadow/*.el
    ;;jabber

    minimap dashboard

    ;;color-theme-sanityinc-tomorrow
    color-theme-modern

    ada-mode elixir-mode julia-mode zig-mode markdown-mode yaml-mode

    company

    ;; these two are relying on 'distel package which is not provided by melpa
    ;;distel-completion-lib auto-complete-distel company-distel

    ;; (modern) alternative to distel (client side)
    ;;erlang-elp

    ;; (modern) alternative to distel (server side)
    ;; lsp-mode
    eglot

    ;; eldoc c-eldoc ivy-erlang-complete

    ;; www.flycheck.org || M-x flycheck-manual
    flymake flymake-easy flycheck flymake-yaml

    popper))

(dolist (pkg my-packages)
  (when (not (package-installed-p pkg))
    (package-install pkg)))

;; some of the packages from above list just needs a basic init

;; https://github.com/zig-lang/zig-mode
(require 'zig-mode)
(add-to-list 'auto-mode-alist '("\\.zig\\'" . zig-mode))

(require 'company)
;; these two needs (require 'distel) which is not provided by melpa
;; (require 'auto-complete-distel)
;; (add-to-list 'ac-sources 'auto-complete-distel)
;; (require 'company-distel)
;; (add-to-list 'company-backends 'company-distel)
;; (setq company-distel-popup-help t)
;; (setq company-distel-popup-height 30)

;; https://github.com/JuliaEditorSupport/julia-emacs
(require 'julia-mode)

;; Homepage: https://github.com/yasuyk/flymake-yaml
(add-hook 'yaml-mode-hook 'flymake-yaml-load)

;;(require 'org)
(setq org-todo-keywords
      '((sequence "TODO" "ACTIVE" "DONE")))

;; popper-mode prefs
(setq popper-reference-buffers
      '(Custom-mode
        compilation-mode
        messages-mode
        help-mode
        occur-mode
        "^\\*Warnings\\*"
        "^\\*Compile-Log\\*"
        "^\\*Messages\\*"
        "^\\*Backtrace\\*"
        "^\\*evil-registers\\*"
        "^\\*Apropos"
        "^Calc:"
        "^\\*ielm\\*"
        "^\\*TeX Help\\*"
        "\\*Shell Command Output\\*"
        "\\*Async Shell Command\\*"
        "\\*Completions\\*"
        "[Oo]utput\\*"))

;;(setq popper-mode-line nil)
;; uncomment next line if you don't want popper to decide where to place
;; popup buffer; in this case popups may take over your regular windows though.
;;(setq popper-display-control 'user)
;; enable popper-mode
(popper-mode 1)

(princ "** ensure-packages-loaded.el -- End\n")
