;; These customizations change the way emacs looks and disable/enable
;; some user interface elements. Some useful customizations are
;; commented out, and begin with the line "CUSTOMIZE". These are more
;; a matter of preference and may require some fiddling to match your
;; preferences

;; Turn off the menu bar at the top of each frame because it's distracting
(menu-bar-mode -1)

;; Show line numbers
(global-display-line-numbers-mode)
;;(global-linum-mode)
(column-number-mode)

;; You can uncomment this to remove the graphical toolbar at the top. After
;; awhile, you won't need the toolbar.
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))

;; Don't show native OS scroll bars for buffers because they're redundant
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

;; Color Themes
;; Read http://batsov.com/articles/2012/02/19/color-theming-in-emacs-reloaded/
;; for a great explanation of emacs color themes.
;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Custom-Themes.html
;; for a more technical explanation.
(add-to-list 'custom-theme-load-path (concat my-emacs-directory "/themes"))
(add-to-list 'load-path (concat my-emacs-directory "/themes"))
;; (load-theme 'tomorrow-night-bright t)
(load-theme 'midnight t)


;; increase font size for better readability
(set-face-attribute 'default nil :height 140)

;; Uncomment the lines below by removing semicolons and play with the
;; values in order to set the width (in characters wide) and height
;; (in lines high) Emacs will have whenever you start it
(setq initial-frame-alist '((top . 2) (left . 0) (width . 122) (height . 32)))
;;(setq initial-frame-alist '((top . 2) (left . 0) (width . 98) (height . 30)))

;; this is for new frames after initial
(setq default-frame-alist '((width . 80) (height . 36)))

;(add-to-list 'default-frame-alist '(font . "Input Mono Condensed"))
(set-frame-font "Input Mono Condensed Light-15")

;; These settings relate to how emacs interacts with your operating system
(setq ;; makes killing/yanking interact with the clipboard
      x-select-enable-clipboard t

      ;; I'm actually not sure what this does but it's recommended?
      x-select-enable-primary t

      ;; Save clipboard strings into kill ring before replacing them.
      ;; When one selects something in another program to paste it into Emacs,
      ;; but kills something in Emacs before actually pasting it,
      ;; this selection is gone unless this variable is non-nil
      save-interprogram-paste-before-kill t

      ;; Shows all options when running apropos. For more info,
      ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Apropos.html
      apropos-do-all t

      ;; Mouse yank commands yank at point instead of at click.
      mouse-yank-at-point t)

;; No cursor blinking, it's distracting
(blink-cursor-mode 1)
(setq blink-cursor-blinks 0)

(setq cursor-type 'bar)

;; Change the cursor shape according to editing mode (insert/overwrite)
(defvar cursor-mode-status 0)
(overwrite-mode 0)
(setq cursor-type 'bar)
(global-set-key (kbd "<insert>")
                (lambda () (interactive)
                  (cond ((eq cursor-mode-status 0)
                         (setq cursor-type 'hbar)
                         (overwrite-mode (setq cursor-mode-status 1)))
                        (t
                         (setq cursor-type 'bar)
                         (overwrite-mode (setq cursor-mode-status 0))))))

;; horizontal line highlight
(hl-line-mode)
;;(set-face-background hl-line-face "gray98")
(set-face-background hl-line-face "gray6")  ;; for dark theme

;; vertical line highlight
;(vline-global-mode)
;(set-face-background 'vline "gray6")
;(set-face-foreground 'vline nil)

;; this is the day-night theme switcher (bound to f2-f5 in my-keybindings)
(defvar day-night-theme-status 1)
(defun day-night-theme ()
  "toggle day/night theme (light/dark)"
  (interactive)
  (cond ((eq day-night-theme-status 0)
         (load-theme 'blippblopp t)
         (setq day-night-theme-status 1)
         (set-face-foreground 'font-lock-comment-delimiter-face "gray53")
         (set-face-foreground 'font-lock-comment-face "gray53")
         (set-face-foreground hl-line-face nil)
         (set-face-background hl-line-face "gray98"))
        ((eq day-night-theme-status 1)
         (load-theme 'desert t)
         (setq day-night-theme-status 2)
         (set-face-foreground 'font-lock-comment-delimiter-face "gray53")
         (set-face-foreground 'font-lock-comment-face "gray53")
         (set-face-background hl-line-face "gray23"))
        (t
         (load-theme 'midnight t)
         (setq day-night-theme-status 0)
         (set-face-foreground 'font-lock-comment-delimiter-face "gray48")
         (set-face-foreground 'font-lock-comment-face "gray48")
         (set-face-background hl-line-face "gray6"))))

;; full path in title bar
(setq-default frame-title-format "%b (%f)")

;; don't pop up font menu
(global-set-key (kbd "s-t") '(lambda () (interactive)))

;; no bell
(setq ring-bell-function 'ignore)
