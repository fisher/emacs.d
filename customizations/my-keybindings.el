;;
;; In the beginning there was some key rebindings...
;;
(princ "** my-keybindings.el -- Begin\n")

;; from now on keys will be grouped

;; (F1 key) hooks to open various smart helper buffers
(global-set-key (kbd "<f1> <f1>")
                (lambda () (interactive) (find-file "~/org/00.org")))
(global-set-key (kbd "<f1> <f2>") 'my-org-files)
(global-set-key (kbd "<f1> <f3>") 'my-erlang-projects)
(global-set-key (kbd "<f1> <f4>") 'my-emacs-config-dir)
(global-set-key (kbd "<f1> <f5>") 'magit-status)

;; (F2 key)
(global-set-key (kbd "<f2> <f1>")
        (lambda () (interactive) (find-file "~/org/ideas.org")))

(global-set-key (kbd "<f2> <f2>")
        (lambda () (interactive) (find-file "~/org/notes.org")))

(global-set-key (kbd "<f2> <f3>")
        (lambda () (interactive) (find-file "~/org/longterm-mem.org")))

(global-set-key (kbd "<f2> <f5>") 'day-night-theme)

;; (F3 key) hooks to open various dired buffers etc
(define-prefix-command 'f3-seq-map)
(global-set-key (kbd "<f3>") 'f3-seq-map)
(define-key f3-seq-map [f4] 'my-current-project)

(define-key f3-seq-map [f3]
  (lambda () (interactive) (find-file "~/org/wishlist.org")))

(define-key f3-seq-map [f2]
  (lambda () (interactive) (find-file "~/org/general-todo.org")))

;; (F7 key) bind to projectile
(define-key projectile-mode-map [f7] 'projectile-command-map)

;; (F8 key) killer key
(define-prefix-command 'f8-seq-map)
(global-set-key (kbd "<f8>") 'f8-seq-map)
(define-key f8-seq-map [f8] (lambda ()
                              (interactive) (kill-buffer nil)))
(define-key f8-seq-map [f10] (lambda ()
			       (interactive) (delete-frame)))

;; misc
(global-set-key [f5] 'flymake-goto-next-error)
(global-set-key [f6] 'flymake-display-err-menu-for-current-line)
(global-set-key [f6] 'flymake-popup-current-error-menu)
(global-set-key [f9] 'buffer-menu)
(global-set-key [f10] 'menu-bar-open)
(global-set-key [f12] 'menu-bar-mode)
(global-set-key [XF86Forward] 'buffer-menu)
(global-set-key [S-XF86Back] 'previous-buffer)
(global-set-key [S-XF86Forward] 'next-buffer)
(global-set-key [C-S-iso-lefttab] 'other-window)
(global-set-key (kbd "C-x !") 'executable-interpret)

(global-set-key (kbd "C-<tab>") (lambda()
                                   (interactive)
                                   (switch-to-buffer nil)))
(global-set-key [backtab] 'ibuffer)

(global-set-key (kbd "C-c r") (lambda ()
                                (interactive)
                                (revert-buffer t t t)
                                (message "buffer is reverted")))

;; TODO: change buffer to nearest *-jabber-chat, if any (or m.b. modified?)
;(global-set-key [M-N] 'next-jabber-event)

(defun my-org-files ()
  "open my org files in $home/org"
  (interactive)
  (find-file "~/org"))

(defun my-erlang-projects ()
  "open my erlang projects directory"
  (interactive)
  (find-file "~/prj"))

(defun my-current-project ()
  "open my current project directory or main project file"
  (interactive)
  (find-file "/home/fisher/prj/larch/tac"))

(defun my-emacs-config-dir ()
  "open .emacs in home directory"
  (interactive)
  (find-file my-emacs-directory))

;; If there is a paren under the cursor, find the pair paren;
;; Otherwise - put itself
;; Obious problem - to insert % in front of a paren, you have
;; to insert space first, then go back one symbol, put %, delete space.
;; If this is a blocker, don't use it.
;; (global-set-key "%" 'match-paren)
(defun match-paren (arg)
  "Go to the matching paren if on a paren; otherwise insert %."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or arg 1)))))

;; I thought it is a default binding for the agenda but no...
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)
(global-set-key (kbd "C-c l") 'org-store-link)

(provide 'my-keybindings)

(princ "** my-keybindings.el -- End\n")
