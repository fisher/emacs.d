# emacs.d

.emacs.d that I use

## rename the cloned repo to ~/.emacs.d

should be enough to get started

## copy scripts `e` and/or `em` to whatever `bin` directory of yours

These scripts are provided as an example how to start emacs.

Please place it into any directory which is available via `$PATH`
environment variable. If you are frustrating right now, just copy `e`
script into `/usr/local/bin`:

```
$ sudo cp e /usr/local/bin
```

But I suggest to add it to `$HOME/bin` and then make sure you have
added this directory to an env variable `$PATH` somewhere in `.bashrc`
or `.zshrc` depending on the shell of your choice.

At this point you should be able to start emacs window ('frame' in
emacs terms) by executing

```
$ e filename.txt
```

...in any directory. This should open existing `filename.txt` or
create a new file with that name, in either new emacs instance or in a
new emacs frame connected to already started emacs instance.

## add keybindings in your window manager to run emacs via scripts `e` and/or `em`

I have my own keybindings to start emacs in two different manners,
with different window shape and fonts, one for quick access to my
notes in `~/org` and another just as a general editor window. You can
do whatever you want, just tweak provided scripts according to your
needs and glue it to whatever key combo you like.

## add `e-t` script to `$GIT_EDITOR` env variable

This script is different from `e`, it doesn't start a new frame and instead opens emacs in text terminal, which is very useful for editing commit message from console with `git commit` command.

```
$ echo 'export GIT_EDITOR=e-t' >> ~/.zshrc
```

And while editing commit message you will have the access to other emacs buffers, i.e. with your code and other open files in emacs.

## consider adding `e` or `e-t` to your `$EDITOR` variable

```
$ echo 'export EDITOR=e' >> ~/.zshrc
```

