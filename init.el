(princ "** init.el -- Begin\n")

;; this is my emacs dir; I use it elsewhere in my config
(setq my-emacs-directory "/home/fisher/.emacs.1.d")

;; this variable seems to be rewritten somewhere,
;; while some addons just ignores it
(setq user-emacs-directory my-emacs-directory)

;; Define package repositories
(require 'package)
(add-to-list 'package-archives
             '("tromey" . "http://tromey.com/elpa/") t)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa-stable" . "http://stable.melpa.org/packages/") t)

(add-to-list 'package-pinned-packages '(cider . "melpa-stable") t)
(add-to-list 'package-pinned-packages '(magit . "melpa-stable") t)

;; Load and activate emacs packages. Do this first so that the
;; packages are loaded before you start trying to modify them.
;; This also sets the load path.
(package-initialize)

;; Download the ELPA archive description if needed.
;; This informs Emacs about the latest versions of all packages, and
;; makes them available for download.
(when (not package-archive-contents)
  (package-refresh-contents))

;; Define he following variables to remove the compile-log warnings
;; when defining ido-ubiquitous
;; (defvar ido-cur-item nil)
;; (defvar ido-default-item nil)
;; (defvar ido-cur-list nil)
;; (defvar predicate nil)
;; (defvar inherit-input-method nil)

;;;;
;; General Customization
;;;;

;; Add a directory to our load path so that when you `load` things
;; below, Emacs knows where to look for the corresponding file.
(add-to-list 'load-path (concat my-emacs-directory "/customizations"))

;; När får jag dig hem vänta, Sven i rosengård?
;; När korpen blir viter, kära moder vår
(load "ensure-packages-loaded.el")

;; https://github.com/emacs-dashboard/emacs-dashboard
(require 'dashboard)
(dashboard-setup-startup-hook)

;; Place downloaded elisp files in ~/.emacs.d/vendor. You'll then be able
;; to load them. Read README.md file there.
(add-to-list 'load-path (concat my-emacs-directory "/vendor"))


(add-to-list 'load-path "/home/fisher/erl/26.0.2/lib/tools-3.6/emacs")
(setq erlang-root-dir "/home/fisher/erl/26.0.2")
(add-to-list 'exec-path "/home/fisher/erl/26.0.2/bin")
(require 'erlang-start)

;; for now it just works, so I'm gonna use it instead of my custom flymake.
;; this is the file from lib/tools/emacs, otp distribution
;; for some reason it doesn't need counterpart shell script
(require 'erlang-flymake)

;; general customization files

;; Sets up exec-path-from-shell so that Emacs will use the correct
;; environment variables
(load "shell-integration.el")

;; These customizations make it easier for you to navigate files,
;; switch buffers, and choose options from the minibuffer.
(load "navigation.el")

;; These customizations change the way emacs looks and disable/enable
;; some user interface elements
(load "ui.el")

;; These customizations make editing a bit nicer.
(load "editing.el")

;; Hard-to-categorize customizations
(load "misc.el")

;; For editing lisps
(load "elisp-editing.el")

;; Langauage-specific
(load "setup-clojure.el")
(load "setup-js.el")

(require 'my-keybindings)

;; here you can store sensitive part of your configuration
;; like account credentials
(add-to-list 'load-path (concat my-emacs-directory "/shadow"))
;;(require 'my-jabber)

;; dumb-jump activation
(add-hook 'xref-backend-functions #'dumb-jump-xref-activate)

(setq coffee-tab-width 4)

(setq minimap-minimum-width 10)
(setq minimap-mode t)
(setq minimap-width-fraction 0.05)
(setq minimap-window-location 'right)

(server-start)

(princ "** init.el -- End\n")
