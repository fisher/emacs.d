;; email configuration

(setq gnus-secondary-select-methods
      '((nnimap "tpaba"
	       (nnimap-address "tpaba.org.ua")
	       (nnimap-server-port 993)
	       (nnimap-stream ssl))))

(add-to-list 'gnus-secondary-select-methods
	     '(nnimap "gmail"
		      (nnimap-address "imap.gmail.com")
		      (nnimap-server-port 993)
		      (nnimap-stream ssl)))

;(setq gnus-select-method
;      '(nnimap "gmail"
;               (nnimap-address "imap.gmail.com")
;               (nnimap-server-port 993)
;               (nnimap-stream ssl)))

; this also works, but with google
;(setq message-send-mail-function 'smtpmail-send-it
;      smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
;      smtpmail-auth-credentials '(("smtp.gmail.com" 587 "valenak@gmail.com" nil))
;      smtpmail-default-smtp-server "smtp.gmail.com"
;      smtpmail-smtp-server "smtp.gmail.com"
;      smtpmail-smtp-service 587
;      smtpmail-local-domain "mycompany.net")

(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-starttls-crenetials '(("mail.tpaba.org.ua" 25 nil nil))
      smtpmail-auth-credentials '(("mail.tpaba.org.ua" 25 "fisher" nil))
      smtpmail-default-smtp-server "mail.tpaba.org.ua"
      smtpmail-smtp-server "mail.tpaba.org.ua"
      smtpmail-smtp-service 25
      smtpmail-local-domain "tpaba.org.ua")

;; Make Gnus NOT ignore [Gmail] mailboxes
(setq gnus-ignored-newsgroups "^to\\.\\|^[0-9. ]+\\( \\|$\\)\\|^[\"]\"[#'()]")

;; i dislike default summary buffer
(setq gnus-summary-line-format "%U%R%z%o %I%(%[%4L: %-23,23f%]%) %s\n")

(provide 'my-email)
