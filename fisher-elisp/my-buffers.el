(require 'ibuffer-vc)

(add-hook 'ibuffer-hook
	  (lambda ()
	    (ibuffer-vc-set-filter-groups-by-vc-root)
	    (unless (eq ibuffer-sorting-mode 'alphabetic)
	      (ibuffer-do-sort-by-alphabetic))))

(setq ibuffer-formats
      '((mark modified read-only vc-status-mini " "
	      (name 18 18 :left :elide)
	      " "
	      (size 9 -1 :right)
	      " "
	      (mode 16 16 :left :elide)
	      " "
	      (vc-status 16 16 :left)
	      " "
	      filename-and-process)))


;(autoload 'ibuffer "ibuffer" "List buffers." t)

;(setq ibuffer-saved-filter-groups
;      (quote (("default"
;	       ;("dired" (mode . dired-mode))
;	       ("perl" (mode . cperl-mode))
;	       ("erc" (mode . erc-mode))
;	       ("planner" (or
;			   (name . "^\\*Calendar\\*$")
;			   (name . "^diary$")
;			   (mode . muse-mode)))
;	       ("emacs" (or
;			 (name . "^\\*scratch\\*$")
;			 (name . "^\\*Messages\\*$")))
;;	       ("track-tmp" (or
;;			     (filename . "/tmp/track-tmp")
;;			     (and
;;			      (mode . dired-mode)
;;			      (name . "^track-tmp$"))))
;	       ("track-tmp" (begin (mode . dired-mode) (name . "^track-tmp$")))
;;	       ("track-tmp" (filename . "/tmp/track-tmp"))
;;	       ("track" (or
;;			 (and
;;			  (mode . dired-mode)
;;			  (name . "^track$"))
;;			 (filename . "/erl/strikead/track/")))
;;	       ("track-v1.4.0" (or
;;				(and
;;				 (mode . dired-mode)
;;				 (name . "^track-v1.4.0$"))
;;				(filename . "/erl/strikead/track-v1.4.0")))
;	       ("gnus" (or
;			(mode . message-mode)
;			(mode . bbdb-mode)
;			(mode . mail-mode)
;			(mode . gnus-group-mode)
;			(mode . gnus-summary-mode)
;			(mode . gnus-article-mode)
;			(name . "^\\.bbdb$")
;			(name . "^\\.newsrc-dribble")))))))
;
;(add-hook 'ibuffer-mode-hook
;	  (lambda ()
;	    (ibuffer-switch-to-saved-filter-groups "default")))
;
(provide 'my-buffers)
