;; жаббер запускается вручную при желании, C-x C-j C-c
;; после запуска - в ростер C-x C-j C-r и дальше природа подскажет

;;(add-to-list 'load-path "/home/username/usr/share/emacs/site-lisp/emacs-jabber-0.7.92")
(add-to-list 'load-path "/usr/share/emacs/site-lisp/emacs-jabber")
(require 'jabber)

(setq jabber-account-list '(
	    ("f1sher@jabber.org/emacs-sofa"
	     (:password . "assword")
	     (:connection-type . ssl) )
;;	    ("fisher@jabber.favoritbet.com/emacs-saws"
;;	     (:password . "assword")
;;	     (:network-server . "jabber.favoritbet.com")
;;	     (:connection-type . starttls) )
            ("fisher@jabber.kiev.ua/emacs-sofa"
             (:password . "assword")
             (:network-server . "jabber.kiev.ua")
             (:connection-type . ssl) )
            ("fisher@jabber.org/emacs-sofa"
             (:password . "assword")
             (:connection-type . ssl) )
	    ("fisher@jabber.ru/emacs-sofa"
	     (:password . "assword")
	     (:connection-type . starttls) )
	))

;; блеять, как же это тяжело было найти!!11
(setq jabber-default-priority 12)

;; мессаги от этих контактов не будут светиться в статусе емакса
;;(add-to-list 'jabber-activity-banned "дринкер")


(provide 'my-jabber)
