
;; вообще-то это левый модуль, с которым ещё нужно разобраться,
;; но из-за релевантности функционала он пока поживёт здесь
(require 'google-calendar)
(setq google-calendar-user "valenak@gmail.com")
(setq google-calendar-code-directory "~/.emacs.d/fisher-elisp/calendar-code")
(setq google-calendar-directory "~/tmp")
(setq google-calendar-url "https://www.google.com/calendar/ical/valenak%40gmail.com/private-43362e97ca7a334fb15a697791ec7997/basic.ics")
(setq google-calendar-auto-update t)

;; собственно орг моде
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(setq org-agenda-files
      (list "~/org/wishlist.org"
            "~/org/general-todo.org"
            "~/org/nextjob-effort.org"
            "~/org/prj/ada.org"
            "~/org/prj/statistics.org"
            "~/org/prj/predict.org"
            "~/org/helpers/orgmode-notes.org"
            "~/org/00.org"))

(add-hook 'org-mode-hook 'turn-on-font-lock)
(add-hook 'org-mode-hook (lambda () (hl-line-mode)))

(provide 'my-org-mode)
