;;-------1---------2---------3---------4---------5---------6---------7---------8
;; configuration for flymake-mode
;; taken from http://www.emacswiki.org/emacs/FlymakeErlang

(require 'flymake)

;; --- R ---

(when (require 'flymake nil)
  (defun flymake-r-init ()
    (let* ((temp-file (flymake-init-create-temp-buffer-copy
                       'flymake-create-temp-inplace))
           (local-file (file-relative-name
                        temp-file
                        (file-name-directory buffer-file-name))))
      (list "~/bin/rflymake" (list local-file))))

  (add-to-list 'flymake-allowed-file-name-masks '("\\.[Rr]\\'" flymake-r-init))
  (add-to-list 'flymake-err-line-patterns
               '("parse(\"\\([^\"]*\\)\"): \\([0-9]+\\):\\([0-9]+\\): \\(.*\\)$"
                 1 2 3 4))

  (add-hook 'ess-mode-hook (lambda ()
                             (flymake-mode t)
                             (hl-line-mode)))
  )

;; --- Perl ---

(add-to-list 'flymake-allowed-file-name-masks '("\\.t" flymake-perl-init))

(defun flymake-perl6-init ()
  (let* ((temp-file
          (flymake-init-create-temp-buffer-copy
           'flymake-create-temp-in-system-tempdir))
         (local-file
          (file-relative-name temp-file
                              (file-name-directory buffer-file-name))))
    (list "perl6" (list "-c " local-file))))

(add-to-list 'flymake-allowed-file-name-masks '("\\.p6" flymake-perl6-init))

;; --- Erlang ---

(defun flymake-create-temp-in-system-tempdir (filename prefix)
  (make-temp-file
   (or prefix "flymake")
   nil
   ;; this is just a basename function for elisp
   (file-name-nondirectory (directory-file-name filename))))

(defun flymake-erlang-init ()
  (let* ((temp-file
          (flymake-init-create-temp-buffer-copy
           'flymake-create-temp-in-system-tempdir))
         (local-file
          (file-relative-name temp-file
                              (file-name-directory buffer-file-name))))
    (list "/home/fisher/bin/flymode-escript" (list local-file))))

(add-to-list 'flymake-allowed-file-name-masks '("\\.hrl$" flymake-erlang-init))
(add-to-list 'flymake-allowed-file-name-masks '("\\.erl$" flymake-erlang-init))

;; --- Perl ---

(add-to-list 'flymake-allowed-file-name-masks '("\\.t" flymake-perl-init))

;; --- elisp ---

(defun flymake-elisp-init ()
  (if (string-match "^ " (buffer-name))
      nil
    (let* ((temp-file   (flymake-init-create-temp-buffer-copy
                         'flymake-create-temp-inplace))
           (local-file  (file-relative-name
                         temp-file
                         (file-name-directory buffer-file-name))))
      (list
       (expand-file-name invocation-name invocation-directory)
       (list
        "-Q" "--batch" "--eval"
        (prin1-to-string
         (quote
          (dolist (file command-line-args-left)
            (with-temp-buffer
              (insert-file-contents file)
              (condition-case data
                  (scan-sexps (point-min) (point-max))
                (scan-error
                 (goto-char(nth 2 data))
                 (princ (format "%s:%s: error: Unmatched bracket or quote\n"
                                file (line-number-at-pos)))))))
          )
         )
        local-file)))))

;(add-to-list 'flymake-allowed-file-name-masks '("\\.el\\'" flymake-elisp-init))
(push '("\\.el$" flymake-elisp-init) flymake-allowed-file-name-masks)
(princ (format "*** %S ***\n" flymake-allowed-file-name-masks))
(add-hook 'emacs-lisp-mode-hook (lambda ()
                                  (flymake-mode t)
                                  (hl-line-mode)))

;; --- C/C++ ---

(add-to-list 'flymake-master-file-dirs "..")

(defvar flymake-additional-compilation-flags nil)
(put 'flymake-additional-compilation-flags 'safe-local-variable 'listp)

;; no need to arrange Makefile
(defun flymake-cc-init ()
  (let* ((temp-file (flymake-init-create-temp-buffer-copy
                     'flymake-create-temp-inplace))
         (local-file (file-relative-name
                      temp-file
                      (file-name-directory buffer-file-name)))
         (common-args (append
;                       (list "-Wall" "-Wextra" "-fsyntax-only"
;                             "-I./" "-D_GNU_SOURCE=1"
;                             "-D_REENTRANT" "-I/usr/include/SDL"
;                             local-file)
                       (list "-Wall" "-Wextra" "-std=c11"
                             "-fsyntax-only" "-fno-show-column"
                             "-I./" "-I./include" "-I../include"
                             "-I/usr/share/R/include"
                             "-I/usr/local/lib/erlang/usr/include"
                             "-D_XOPEN_SOURCE=600"
                             "-pedantic" local-file)
                       flymake-additional-compilation-flags)))
    (if (eq major-mode 'c++-mode)
        (list "g++" common-args)
      (list "gcc" common-args))))

(cl-loop for ext in '("\\.c$" "\\.h$" "\\.cc$" "\\.cpp$" "\\.hh$")
      do
      (push `(,ext flymake-cc-init) flymake-allowed-file-name-masks))

(add-hook 'c-mode-hook (lambda ()
                         (flymake-mode t)
                         (setq c-basic-offset 4)
                         (hl-line-mode)))
(add-hook 'c++-mode-hook
          (lambda ()
            (hl-line-mode)
            (flymake-mode t)))


(provide 'my-flymake)
