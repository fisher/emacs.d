;; distel, according to their README, needs these:
;; (add-to-list 'load-path "/home/fisher/lib/share/distel/elisp")
(add-to-list
 'load-path
 (car (file-expand-wildcards "/usr/local/lib/erlang/lib/tools-*/emaccs")))
(add-to-list 'load-path "/usr/local/share/distel/elisp")

(require 'distel)
(distel-setup)

;; I've set it up 25.08.2017
;; and commented out above 3 lines
;(add-to-list 'load-path
;             (car (file-expand-wildcards
;                   "/usr/local/lib/erlang/lib/wrangler-*/elisp")))
;(add-to-list 'load-path "/usr/local/lib/erlang/lib/wrangler-1.2.0/elisp")
;(require 'wrangler)

;(add-hook 'after-init-hook 'my-after-init-hook)
;(defun my-after-init-hook ()
;  (require 'edts-start))

(provide 'my-distel)
